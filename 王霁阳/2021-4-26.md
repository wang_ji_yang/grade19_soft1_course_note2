# 2021年4月26日课堂笔记

为什么会这么久不做课堂笔记，因为，不知道做什么，是真的不知道做什么，不过从今天开始，我把我能踩得坑都写上

### ajax提交表单
```
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Insert title here</title>
</head>
<body>
<!--  -->
<form  method="post" onsubmit="return false" action="##" id="formtest">

	username:<input type="text" name="username" /><br>
	
	password:<input type="password" name="password" /> <br>
	
	<input type="button" value="登录" onclick="login()">

</form>
	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/js/extJquery.js"></script>
	<script type="text/javascript" src="/js/extEasyUI.js"></script>
<script type="text/javascript">
function login() {
    $.ajax({
    //几个参数需要注意一下
        type: "POST",//方法类型
        dataType: "text",//预期服务器返回的数据类型 如果是对象返回的是json 如果是字符串这里一定要定义text 之前我就是定义json 结果字符串的返回一直到额error中去
        /*
        dataType:
			要求为String类型的参数，预期服务器返回的数据类型。如果不指定，JQuery将自动根据http包mime信息返回responseXML或responseText，并作为回调函数参数传递。可用的类型如下：
			xml：返回XML文档，可用JQuery处理。
			html：返回纯文本HTML信息；包含的script标签会在插入DOM时执行。
			script：返回纯文本JavaScript代码。不会自动缓存结果。除非设置了cache参数。注意在远程请求时（不在同一个域下），所有post请求都将转为get请求。
			json：返回JSON数据。
			jsonp：JSONP格式。使用SONP形式调用函数时，例如myurl?callback=?，JQuery将自动替换后一个“?”为正确的函数名，以执行回调函数。
			text：返回纯文本字符串。
        */
        url: "http://localhost:8080/user",//url
        data: $('#formtest').serialize(),//这个是form表单中的id   jQuery的serialize()方法通过序列化表单值
        success: function (result) {
        	alert("成功")
            console.log(result);//打印服务端返回的数据(调试用)
            if (result.resultCode == 200) {
                alert("SUCCESS");
            }
            ;
        },
        error : function(s,s2,s3) {
			//数据成功传到后台 也有返回值 但就是报错 parsererror ：参考
			https://blog.csdn.net/AinGates/article/details/75250223 / 
			https://blog.csdn.net/AinGates/article/details/75250223
        	/*
        	    写了一个ajax方法，后台一切正常，通过浏览器的F12工具查看XMLHttpRequest.status返回200，XMLHttpRequest.readyState返回4，也都没有问题。但是回调函数跳到error里，报parsererror的错误。经过排查，发现是因为后台返回时用了@ResponseBody注解（SpringMVC返回json格式的注解），但前台ajax提交没有定义dataType属性（定义服务器返回的数据类型）

			    还有一种情况是ajax方法中定义了 dataType:"json"属性，就一定要返回标准格式的json字符串，要不jQuery1.4+以上版本会报错的，因为不是用eval生成对象了，用的JSON.parse，如果字符串不标准就会报错。比如只返回一个简单的字符串“success”，“fail”， true，false，并不是标准的json字符串就会报错。
			
		               首先，jQuery 1.4版本之后对服务端返回的JSON 数据要求比较严格，必须严格按照JSON的标准来了。
        	*/
			
			console.log(s)
        	console.log(s2)
        	console.log(s3)
        	
            alert("异常！");
        }
    });
}
</script>

</body>

</html>

```

controller类的代码不变，启动项目并访问http://localhost:8080/query.html 返货成功即可

这里遇到一个问题：就是传过去数据后，返回值正常，但进入了error ，使用console打印error时出现 parsererror

错误原因：ajax的datatype设置问题 我之前设置为 json ,这种只能是接收后台传回来的json值 传回其他值就会出现这种错误

解决办法：将datatype的类型改为 text
————————————————
版权声明：本文为CSDN博主「初出茅庐的小白0101」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/a1194821568/article/details/90762922