# 2021年3月29日课堂笔记

今天胡哥讲的有点没听懂，慢慢来了，不能急啊

## 封装一整个文件

把index.js全部封装，然后在main.js里调用，这样代码非常的简洁，好浏览，代码如下

index.js

```
//引入所需要的模块
let fs = require('fs');
let path = require('path');
let router=require('koa-router')();
function knowledge(){
    //获取控制器的相对路径
    let defaultControllerDir = 'controllers' || "";

    //获取控制器的绝对路径
    let root = path.resolve('.');

    //连接相对路径与绝对路径，获得我们真正所需路径

    let defaultControllerDirRootPath = path.join(root,defaultControllerDir);

    //打开绝对路径

    let files = fs.readdirSync(defaultControllerDirRootPath);

    //筛选所需的文件，不要本文件
    let resultFiles  = files.filter((item)=>{
        return item.endsWith('.js') && item!=='index.js';
    })
    //循环文件名，目的是让每一个文件都暴露出来
    resultFiles.forEach((item)=>{
        //链接文件与文件夹路径
    let file = path.join(defaultControllerDirRootPath,item);

    //打开文件模块
    let fileobj = require(file);

    //循环配置
    for(let r in fileobj){
        let type = fileobj[r][0];
        let fn = fileobj[r][1];
        if(type=='get'){
            router.get(r,fn)
        }else{
            router.post(r,fn)
        }
    }
    
    })
}
module.exports=knowledge;
```

mian.js

```
//引入模块

let Koa = require('koa');
let router = require('koa-router')();
let bodyParser = require('koa-bodyparser');
let app = new Koa();

app.use(bodyParser());

let indexRouter = require('./controllers/index')

app.use(indexRouter());


// 定义端口，设置监听
let port = 3000;
app.listen(port);

// 打印服务器信息
console.log(`http://127.0.0.1:${port}`);
```