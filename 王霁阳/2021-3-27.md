## 2021年3月27日课堂笔记

胡哥今天说了，以后课堂练习也可以放在自己的远程仓库里，让我们过多年以后看看自己写的bug

## 今日所学知识
今天胡哥主要还是复习了之前的课程，关于重构还更深的讲了一点点
以下是users.js的代码

```
let list_fn = async(ctx,next)=>{
    ctx.body="这是个人信息首页"
}
let add_fn = async(ctx,next)=>{
    ctx.body="这是添加界面"
}
let delete_fn = async(ctx,next)=>{
    ctx.body="这是删除界面"
}
let modification_fn = async(ctx,next)=>{
    ctx.body="这是修改界面"
}
let details_fn= async(ctx,next)=>{
    ctx.body="这是详情界面"
}
module.exports={
    '/users/list':['get',list_fn],
    '/users/add':['post',add_fn],
    '/users/delete':['delete',delete_fn],
    '/users/mpdification':['put',delete_fn],
    '/users/details':['get',details_fn]
}
```

以下是article.js的代码
```
let list_fn = async(ctx,next)=>{
    ctx.body="这是文章信息首页"
}
let add_fn = async(ctx,next)=>{
    ctx.body="这是添加界面"
}
let delete_fn = async(ctx,next)=>{
    ctx.body="这是删除界面"
}
let modification_fn = async(ctx,next)=>{
    ctx.body="这是修改界面"
}
let details_fn= async(ctx,next)=>{
    ctx.body="这是文章详情界面"
}
module.exports={
    '/article/list':['get',list_fn],
    '/article/add':['post',add_fn],
    '/article/delete':['delete',delete_fn],
    '/article/mpdification':['put',delete_fn],
    '/article/details':['get',details_fn]
}
```

以下是index.js的代码
```
//引入fs模块和path模块

let fs = require('fs');

let path = require('path');

//利用fs模块，打开存放网页逻辑的文件夹
console.log(__dirname);
var files = fs.readdirSync(__dirname)

console.log(files);

files.forEach((item) => {
    //排除函数本身
    if (item != 'index.js' && item.endsWith('.js')) {
        let tmpFiles = path.join(__dirname, item);
        let tmpRoutes = require(tmpFiles);
        //打开模块，取出需要的值
        for (let r in tmpRoutes) {
            console.log(r);
            //获得键值对中的值，里面的请求类型
            let type = routes[x][0];
            // 获取键值中的值（是一个数组），里面的处理函数
            let fn = routes[x][1];
            if (type == 'get') {
                router.get(x, fn);
            } else {
                router.post(x, fn);
            }
        }
    }
})
```
关于为什么index要和处理URL的函数放在同一文件夹的问题，希望胡哥下节课能讲解讲解。