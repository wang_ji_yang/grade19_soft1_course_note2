# 2021年3月23日

今日所学知识，胡哥介绍了一下yarn和处理post请求

## yarn

```
“Yarn是由Facebook、Google、Exponent 和 Tilde 联合推出了一个新的 JS 包管理工具 ，正如官方文档中写的，Yarn 是为了弥补 npm 的一些缺陷而出现的。

npm install的时候巨慢。特别是新的项目拉下来要等半天，删除node_modules，重新install的时候依旧如此。
同一个项目，安装的时候无法保持一致性。由于package.json文件中版本号的特点，下面三个版本号在安装的时候代表不同的含义。

```
而且，npm用的是同步下载，所以会很慢，而yarn用的是异步

### yarn优点

![图裂了！](./images/2021-03-23_Yarn.png)

## 处理post请求

用router.get('/path', async fn)处理的是get请求。如果要处理post请求，可以用router.post('/path', async fn)。

用post请求处理URL时，我们会遇到一个问题：post请求通常会发送一个表单，或者JSON，它作为request的body发送，但无论是Node.js提供的原始request对象，还是koa提供的request对象，都不提供解析request的body的功能！

所以，我们又需要引入另一个middleware来解析原始request请求，然后，把解析后的参数，绑定到ctx.request.body中。

koa-bodyparser就是用来干这个活的。

我们在package.json中添加依赖项：
```
"dependencies": {
        "koa":"latest",
        "koa-router":"latest",
        "koa-bodyParser":"latest"
    }
```
由于middleware的顺序很重要，这个koa-bodyparser必须在router之前被注册到app对象上。


代码如下

```
"use scrict"
//创建模块
let Koa = require("Koa");
let router = require("koa-router")();
let bodyparser = require("koa-bodyparser");
//开始玩
let app = new Koa();
app.use(bodyparser());
//链接
app.use(router.routes());

router.get('/', async (ctx, next) => {
    ctx.body = "我爱她轰轰烈烈最疯狂！！"
})
router.get('/hello', async (ctx, next) => {
    ctx.body = "Hello"
})
router.get('/hello/:name', async (ctx, next) => {
    var name = ctx.params.name;
    ctx.body = `hello,${name}`
})
router.get('/login', async (ctx, next) => {
    ctx.response.body = `
    <form action="/login/loginCone" method="POST">
    <label>用户名：<input name="userName" type="text"></label>
    <br>
    <label>密 码：<input name="passWord" type="password"</label>
    <br>
    <input type="submit" value="登入">
    </form>
    `
})
router.post('/login/loginCone',async(ctx,next)=>{
    //获取表单的传输数据
    let 
        userName = ctx.request.body.userName || "",
        passWord = ctx.request.body.passWord || "";
    console.log(userName);
    console.log(passWord);
    if(userName==='root' && passWord===123){
        //密码对了
        ctx.body="成功"
    }else{
        //出错
        ctx.body="用户名或密码出错"
    }
})
let sort = 4000;
app.listen(sort)

console.log(`http://127.0.0.1:${sort}`);
```

要注意的是：由于middleware的顺序很重要，这个koa-bodyparser必须在router之前被注册到app对象上。